from django.db import models

# Create your models here.
class News(models.Model):
    judul_news = models.CharField(max_length=100)
    desc_news = models.CharField(max_length=1024)
    date_news = models.DateField(null=True)
    categories_news = models.CharField(max_length=20,null=True)
    image_news = models.ImageField(upload_to='media/images/',max_length=1024,null=True)
    source_news = models.CharField(max_length=30,null=True)

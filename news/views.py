from django.shortcuts import render, redirect
from .models import News
from django.http import Http404
# Create your views here.
def news(request):
    list_news = News.objects.all()
    return render(request, 'news.html', {'list_news':list_news})

def slug(request, page_slug):
    try:
        news = News.objects.get(judul_news=page_slug)
    except:
        raise Http404('News doesnt exist')
    return render(request, 'id.html', {'news':news})

def id(request, news_id):
    try:
        news = News.objects.get(pk=news_id)
    except:
        raise Http404('News doesnt exist')
    return redirect('/news/'+news.judul_news)

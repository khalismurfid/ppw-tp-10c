from django.urls import path
from . import views

urlpatterns = [
    path('', views.news, name="news"),
    path('<page_slug>/', views.slug, name='news_slug'),
    path('id/<int:news_id>/', views.id, name='news_id'),
]

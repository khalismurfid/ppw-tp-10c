from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import dashboard
# Create your tests here.

class DashboardTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        
    def test_random_url_is_non_exist(self):
        response = Client().get('dsadawdwadawdwadawdawd')
        self.assertNotEqual(response.status_code , 200)
        
    def test_dashboard_using_dashboard_func(self):
        function = resolve('/')
        self.assertEqual(function.func, dashboard)


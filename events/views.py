from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import random
from .models import Events
from django.http import Http404
# Create your views here.

def id(request, events_id):
    list_events=Events.objects.all().order_by('?')[:3]
    try:
        events = Events.objects.get(pk=events_id)
    except:
        raise Http404('Events doesnt exist')
    return render(request, 'particular.html', {'events':events,'list_events':list_events})

class ArticlesView(ListView):
    model = Events
    paginate_by = 5
    context_object_name = 'list_events'
    template_name = 'events.html'

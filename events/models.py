from django.db import models

# Create your models here.
class Events(models.Model):
    judul_events = models.CharField(max_length=100)
    venue_events = models.CharField(max_length=1024)
    desc_events = models.CharField(max_length=1024)
    date_events = models.DateField()
    image = models.CharField(max_length=100, default="https://www.newstatesman.com/sites/all/themes/creative-responsive-theme/images/new_statesman_events.jpg")

    class Meta:
        ordering = ['date_events']
